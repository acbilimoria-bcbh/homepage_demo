Rails.application.routes.draw do
  root 'welcome#index'
  match '/top_secret', to: 'welcome#top_secret', via: :get
end
